# **International Business Men**

Buenos días, 

No he realizado la prueba por distintos motivos. La prueba y su planteamiento, me han mostrado que ni yo soy el perfil de trabajador que se busca, ni este el puesto que preciso.

Después de años de experiencias laborales, valoro mucho el respeto por el tiempo personal de ocio, la sinceridad y la transparencia en una relación laboral.

En primer lugar, leí con detenimiento el enunciado de la prueba e hice un pequeño análisis de qué se espera y como lo plantearia, localizando los puntos 'a priori' más complejos o costosos. Ahora bien, no la realicé por:

1- La prueba se plantea con un tiempo máximo de dedicación de 4 horas. 
Este hito no es posible con los requisitos de calidad que se exigen, con escaso tiempo para su análisis, sumado a las dificultades ocultas en algunos algoritmos, sin tiempo material para buscar información, ni a realizar pruebas.
Veo imposible conseguir el resultado mínimo deseado en el máximo tiempo a invertir, con un pequeño análisis, la estructuración, la preparación de los proyectos, la definición e implementación de  políticas de tratamiento de logs y excepciones, el sistema offline de cambio de divisas, el algoritmo para encontrar el mejor cambio 'no directo' de divisa, etc

2- La prueba también se plantea como una demostración del conocimiento del candidato, para que la empresa pueda evaluar su valía.
Ahora bien, para realizar correctamente la prueba se debería invertir más tiempo del máximo exigido: o bien se demuestra el conocimiento del candidato falseando la inversión en tiempo real, o bien se realiza parcialmente la prueba ocultando su conocimiento y experiencia. 

3- El plazo de entrega de la prueba para demostrar la valía del candidato no ha tenido en cuenta su disponibilidad para su dedidación.

Como ya he comentado, creo que no soy el candidato que se está buscando: no deseo sentar las bases de una nueva relación laboral con la realización de una prueba que implique el sacrificio no acordado de mi tiempo libre a la vez que falsea mis conocimientos reales o bien falsea mi dedicación real. 

Muchas gracias por la confianza mostrada en mi, y siento no ser el candidato apropiado.

Un saludo,

_Jordi Borràs i Llossas_

